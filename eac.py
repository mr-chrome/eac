# -*- coding: utf-8 -*-

import os as os
from os.path import expanduser
import json

try:
    from StringIO import StringIO
except Exception as e:
    from io import StringIO

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QMainWindow, QTextEdit, QAction, QFileDialog, QApplication


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):

        # UTILS

        def fileDialog():
            default_folder = expanduser("~")
            filter = "txt (*.txt)"
            fname = QFileDialog.getOpenFileName(self, 'Open file', default_folder, filter)

            if fname[0]:
                return(fname[0])

        def folderDialog():
            default_folder = expanduser("~")
            fname = QFileDialog.getExistingDirectory(self, 'Open Folder', default_folder)
            if fname:
                return(fname)

        def resetStatus():
            # Set labels to 'Ready.'
            lbls_arr = [
            self.label_status,
            self.label_status_2
            ]

            self.label_status.setText('Ready.')
            self.label_status_2.setText('Ready.')

        def resetInputs():

            # Reset Status Label when resetting inputs
            resetStatus()

            # Collect inputs
            inputs_arr = [
            self.input_comment,
            self.input_comment_2,
            self.input_selectFile_in,
            self.input_selectFolder_in,
            self.input_selectFolder_outFolder,
            self.input_selectFolder_outFile,
            self.listWidget_folder,
            self.textEdit_singleFile,
            self.lineEdit_fileName
            ]

            # Clear inputs
            for element in inputs_arr:
                element.clear()

        def getDate():
            temp_var = self.dateEdit_date
            if temp_var.date():
                return(str(temp_var.date().toPyDate()))
            else:
                return("Unknown Date")

        def getComment():
            temp_var = self.input_comment.text()
            if temp_var:
                return(temp_var)
            else:
                return("No comment")

        def previewGen(textfile, n):
            # Prende le prime n righe di un file di testo e crea uno stringIO con quelle
            strIO = StringIO()
            counter=0
            if n==0: # Set n = 0 to read all file
                for line in textfile:
                    strIO.write(line)
                return(strIO)
            else:
                for line in textfile:
                    # Watch out: if n > number of lines in text, the function will resolve in an error!
                    if counter < n: # n > 0 to get a specific number of lines
                        strIO.write(line)
                        counter+=1
                    else:
                        return(strIO)

        # MAIN
        def outFolder_single():
            self.input_selectFolder_outFile.setText(str(folderDialog()))

        def outFolder_folder():
            self.input_selectFolder_outFolder.setText(str(folderDialog()))

        def openFolder():
            resetInputs()
            folder_path = str(folderDialog())
            self.input_selectFolder_in.setText(folder_path)
            self.input_selectFolder_outFolder.setText(os.path.join(folder_path, 'output'))

            try:
                ascii_files = [x for x in os.listdir(folder_path) if x.endswith(".txt")] #Make list of txt files in directory
                if len(ascii_files) > 0:
                    for file in ascii_files:
                        self.listWidget_folder.addItem(str(file))
                    self.label_status.setText('Files loaded!')
                else:
                    self.label_status.setText('Files not found!')
            except:
                self.label_status.setText('This directory doesn\'t exists!')

        def openFile():
            resetInputs()
            file_path = str(fileDialog()) # Get file with a Dialog
            path, filename = os.path.split(file_path) # Get directory and filename as distinct vars

            self.input_selectFile_in.setText(file_path)
            self.input_selectFolder_outFile.setText(os.path.join(path, 'output'))
            self.lineEdit_fileName.setText(os.path.splitext(filename)[0]) # Remove extension

            try:
                file = open(file_path,'r') # Open file
                self.label_status_2.setText('File loaded!')

                try:
                    file_preview = previewGen(file, 0) # Extract first n lines
                    file_preview.seek(0) # seek to start
                    self.textEdit_singleFile.setText(file_preview.read()) # Print in textEdit
                    file.close()
                except:
                    self.textEdit_singleFile.setText('Error in file reading. Try again or select another file\n¯\\_(ツ)_/¯')
                    file.close()
            except:
                print("Il file selezionato non esiste.")

        def collectFromAscii(inpath):
            # Colleziona i dati in un dizionario
            dataset = {
            'f_list': [], # field
            'i_list': [] # intensity
            }

            # Open the original ASCII file
            f = open(inpath, 'r')
            # For each line in the file:
            for line in f:
                line = line.strip() # Elimina caratteri invisibili
                columns = line.split() # Trasforma la stringa in una lista di elementi
                if len(columns) == 3: # Se la lista ha esattamente tre elementi...
                    # Aggiungi alla lista dei campi quello di questa linea
                    field = columns[1]
                    dataset['f_list'].append(field)
                    # Fai lo stesso con l'intensita'
                    intensity = columns[2]
                    dataset['i_list'].append(intensity)

            f.close()

            dataset['points'] = len(dataset['i_list']) ## Number of spectrum points

            # Compare number of points and get main intensity values
            if len(dataset['f_list']) == dataset['points']:
                dataset['f_start'] = dataset['f_list'][0] ## Get first intensity value
                dataset['f_end'] = dataset['f_list'][dataset['points']-1] ## Get last intensity value
            else:
                print('Warning: there is something wrong with number of points!')
                return ## Stop the function

            return(dataset)

        def generateEsr(inpath, outpath):
            # Make long column-string with a list of points
            ni_list = []
            dataset = collectFromAscii(inpath)
            for element in dataset['i_list']:
                ni_list.append(element + '\n')

            n = open(outpath, 'w') ## Open the new file

            # Write frontmatter
            n.write('***************\n')
            n.write(getDate() + '\n')
            n.write(dataset['f_start'] + '\n')
            n.write(dataset['f_end'] + '\n')
            n.write(str(dataset['points']) + '\n')
            n.write(getComment() + '\n')
            n.write('***************\n')

            # Write intensities
            n.writelines(ni_list)

            n.close() ## Close the new file

            print('Success!')

        def generateJSON(inpath, outpath):
            dataset = collectFromAscii(inpath)
            n = open(outpath, 'w') ## Open the new file
            json.dump(dataset, n)
            n.close()
            print('Success!')

        def singleFileGen():
            extension = str(self.comboBox_format.currentText())
            inp = self.input_selectFile_in.text()
            filename = str(self.lineEdit_fileName.text()) + extension
            out_path = self.input_selectFolder_outFile.text()

            if not os.path.exists(out_path): # Make dir if not exists
                os.makedirs(out_path)
            outp = os.path.join(out_path, filename)

            if extension.endswith('.esr'):
                generateEsr(inp, outp)
                self.label_status_2.setText('File converted (ESR)!')
            elif extension.endswith('.json'):
                generateJSON(inp, outp)
                self.label_status_2.setText('File converted (JSON)!')
            else:
                self.label_status_2.setText('Error during conversion: try again!')

        def multiFileGen():
            counter = 0
            extension = '.' + str(self.comboBox_folderFormat.currentText())
            path = self.input_selectFolder_in.text()
            items = [str(self.listWidget_folder.item(i).text()) for i in range(self.listWidget_folder.count())]
            out_path = self.input_selectFolder_outFolder.text()

            if not os.path.exists(out_path): # Make dir if not exists
                os.makedirs(out_path)

            for item in items:
                inp = os.path.join(path, item)
                filename = os.path.splitext(item)[0] + extension
                outp = os.path.join(out_path, filename)

                if extension.endswith('.esr'):
                    generateEsr(inp, outp)
                elif extension.endswith('.json'):
                    generateJSON(inp, outp)
                else:
                    self.label_status.setText('Error during conversion: try again!')

                counter+=1
                self.label_status.setText('File successfully converted! x' + str(counter))

        def bridges():
            # Upper
            self.btn_selectFile_in.clicked.connect(openFile)
            self.btn_selectFolder_in.clicked.connect(openFolder)

            # Central
            self.btn_selectFolder_out_2.clicked.connect(outFolder_single)
            self.btn_selectFolder_outFolder.clicked.connect(outFolder_folder)

            # Lower
            self.btn_convert_2.clicked.connect(singleFileGen)
            self.btn_convert.clicked.connect(multiFileGen)

            # Actions
            self.actionReset_Input_Boxes.triggered.connect(resetInputs)
            self.action_Quit.triggered.connect(QApplication.quit)

        ## GUI STUFF

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(506, 635)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.label_title = QtWidgets.QLabel(self.centralwidget)
        self.label_title.setAlignment(QtCore.Qt.AlignCenter)
        self.label_title.setObjectName("label_title")
        self.gridLayout.addWidget(self.label_title, 0, 0, 1, 1)
        self.tabs_main = QtWidgets.QTabWidget(self.centralwidget)
        self.tabs_main.setObjectName("tabs_main")
        self.tab_single_file = QtWidgets.QWidget()
        self.tab_single_file.setObjectName("tab_single_file")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.tab_single_file)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.frame_single_file = QtWidgets.QFrame(self.tab_single_file)
        self.frame_single_file.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_single_file.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_single_file.setObjectName("frame_single_file")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.frame_single_file)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.group_selectFile_in = QtWidgets.QGroupBox(self.frame_single_file)
        self.group_selectFile_in.setObjectName("group_selectFile_in")
        self.gridLayout_10 = QtWidgets.QGridLayout(self.group_selectFile_in)
        self.gridLayout_10.setObjectName("gridLayout_10")
        self.btn_selectFile_in = QtWidgets.QPushButton(self.group_selectFile_in)
        self.btn_selectFile_in.setObjectName("btn_selectFile_in")
        self.gridLayout_10.addWidget(self.btn_selectFile_in, 0, 0, 1, 1)
        self.input_selectFile_in = QtWidgets.QLineEdit(self.group_selectFile_in)
        self.input_selectFile_in.setObjectName("input_selectFile_in")
        self.gridLayout_10.addWidget(self.input_selectFile_in, 0, 1, 1, 1)
        self.textEdit_singleFile = QtWidgets.QTextEdit(self.group_selectFile_in)
        self.textEdit_singleFile.setObjectName("textEdit_singleFile")
        self.gridLayout_10.addWidget(self.textEdit_singleFile, 1, 0, 1, 2)
        self.gridLayout_2.addWidget(self.group_selectFile_in, 0, 0, 1, 1)
        self.group_optionals_2 = QtWidgets.QGroupBox(self.frame_single_file)
        self.group_optionals_2.setObjectName("group_optionals_2")
        self.gridLayout_7 = QtWidgets.QGridLayout(self.group_optionals_2)
        self.gridLayout_7.setObjectName("gridLayout_7")
        self.dateEdit_date_2 = QtWidgets.QDateEdit(self.group_optionals_2)
        self.dateEdit_date_2.setMinimumDateTime(QtCore.QDateTime(QtCore.QDate(2000, 1, 1), QtCore.QTime(0, 0, 0)))
        self.dateEdit_date_2.setMinimumDate(QtCore.QDate(2000, 1, 1))
        self.dateEdit_date_2.setDateTime(QtCore.QDateTime.currentDateTime()) # Mine
        self.dateEdit_date_2.setObjectName("dateEdit_date_2")
        self.gridLayout_7.addWidget(self.dateEdit_date_2, 0, 0, 1, 1)
        self.input_comment_2 = QtWidgets.QLineEdit(self.group_optionals_2)
        self.input_comment_2.setObjectName("input_comment_2")
        self.gridLayout_7.addWidget(self.input_comment_2, 1, 0, 1, 1)
        self.gridLayout_2.addWidget(self.group_optionals_2, 1, 0, 1, 1)
        self.group_selectFolder_out_2 = QtWidgets.QGroupBox(self.frame_single_file)
        self.group_selectFolder_out_2.setObjectName("group_selectFolder_out_2")
        self.gridLayout_8 = QtWidgets.QGridLayout(self.group_selectFolder_out_2)
        self.gridLayout_8.setObjectName("gridLayout_8")
        self.btn_selectFolder_out_2 = QtWidgets.QPushButton(self.group_selectFolder_out_2)
        self.btn_selectFolder_out_2.setObjectName("btn_selectFolder_out_2")
        self.gridLayout_8.addWidget(self.btn_selectFolder_out_2, 0, 0, 1, 1)
        self.input_selectFolder_outFile = QtWidgets.QLineEdit(self.group_selectFolder_out_2)
        self.input_selectFolder_outFile.setObjectName("input_selectFolder_outFile")
        self.gridLayout_8.addWidget(self.input_selectFolder_outFile, 0, 1, 1, 2)
        self.label_fileName = QtWidgets.QLabel(self.group_selectFolder_out_2)
        self.label_fileName.setObjectName("label_fileName")
        self.gridLayout_8.addWidget(self.label_fileName, 1, 0, 1, 1)
        self.lineEdit_fileName = QtWidgets.QLineEdit(self.group_selectFolder_out_2)
        self.lineEdit_fileName.setObjectName("lineEdit_fileName")
        self.gridLayout_8.addWidget(self.lineEdit_fileName, 1, 1, 1, 1)
        self.comboBox_format = QtWidgets.QComboBox(self.group_selectFolder_out_2)
        self.comboBox_format.setObjectName("comboBox_format")
        self.comboBox_format.addItem("")
        self.comboBox_format.addItem("")
        self.gridLayout_8.addWidget(self.comboBox_format, 1, 2, 1, 1)
        self.gridLayout_2.addWidget(self.group_selectFolder_out_2, 2, 0, 1, 1)
        self.groupBox_execute_2 = QtWidgets.QGroupBox(self.frame_single_file)
        self.groupBox_execute_2.setTitle("")
        self.groupBox_execute_2.setObjectName("groupBox_execute_2")
        self.gridLayout_9 = QtWidgets.QGridLayout(self.groupBox_execute_2)
        self.gridLayout_9.setObjectName("gridLayout_9")
        self.btn_convert_2 = QtWidgets.QPushButton(self.groupBox_execute_2)
        self.btn_convert_2.setMaximumSize(QtCore.QSize(182, 16777215))
        self.btn_convert_2.setObjectName("btn_convert_2")
        self.gridLayout_9.addWidget(self.btn_convert_2, 0, 0, 1, 1)
        self.label_status_2 = QtWidgets.QLabel(self.groupBox_execute_2)
        self.label_status_2.setAlignment(QtCore.Qt.AlignCenter)
        self.label_status_2.setObjectName("label_status_2")
        self.gridLayout_9.addWidget(self.label_status_2, 1, 0, 1, 1)
        self.gridLayout_2.addWidget(self.groupBox_execute_2, 3, 0, 1, 1)
        self.verticalLayout_2.addWidget(self.frame_single_file)
        self.tabs_main.addTab(self.tab_single_file, "")
        self.tab_folder = QtWidgets.QWidget()
        self.tab_folder.setObjectName("tab_folder")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.tab_folder)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.frame_folder = QtWidgets.QFrame(self.tab_folder)
        self.frame_folder.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_folder.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_folder.setObjectName("frame_folder")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.frame_folder)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.group_selectFolder_in = QtWidgets.QGroupBox(self.frame_folder)
        self.group_selectFolder_in.setObjectName("group_selectFolder_in")
        self.gridLayout_11 = QtWidgets.QGridLayout(self.group_selectFolder_in)
        self.gridLayout_11.setObjectName("gridLayout_11")
        self.btn_selectFolder_in = QtWidgets.QPushButton(self.group_selectFolder_in)
        self.btn_selectFolder_in.setObjectName("btn_selectFolder_in")
        self.gridLayout_11.addWidget(self.btn_selectFolder_in, 0, 0, 1, 1)
        self.input_selectFolder_in = QtWidgets.QLineEdit(self.group_selectFolder_in)
        self.input_selectFolder_in.setObjectName("input_selectFolder_in")
        self.gridLayout_11.addWidget(self.input_selectFolder_in, 0, 1, 1, 1)
        self.listWidget_folder = QtWidgets.QListWidget(self.group_selectFolder_in)
        self.listWidget_folder.setObjectName("listWidget_folder")
        self.gridLayout_11.addWidget(self.listWidget_folder, 1, 0, 1, 2)
        self.gridLayout_4.addWidget(self.group_selectFolder_in, 0, 0, 1, 1)
        self.group_selectFolder_out = QtWidgets.QGroupBox(self.frame_folder)
        self.group_selectFolder_out.setObjectName("group_selectFolder_out")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.group_selectFolder_out)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.btn_selectFolder_outFolder = QtWidgets.QPushButton(self.group_selectFolder_out)
        self.btn_selectFolder_outFolder.setObjectName("btn_selectFolder_outFolder")
        self.gridLayout_3.addWidget(self.btn_selectFolder_outFolder, 0, 0, 1, 1)
        self.input_selectFolder_outFolder = QtWidgets.QLineEdit(self.group_selectFolder_out)
        self.input_selectFolder_outFolder.setObjectName("input_selectFolder_outFolder")
        self.gridLayout_3.addWidget(self.input_selectFolder_outFolder, 0, 1, 1, 1)
        self.comboBox_folderFormat = QtWidgets.QComboBox(self.group_selectFolder_out)
        self.comboBox_folderFormat.setObjectName("comboBox_folderFormat")
        self.comboBox_folderFormat.addItem("")
        self.comboBox_folderFormat.addItem("")
        self.gridLayout_3.addWidget(self.comboBox_folderFormat, 0, 2, 1, 1)
        self.gridLayout_4.addWidget(self.group_selectFolder_out, 2, 0, 1, 1)
        self.groupBox_execute = QtWidgets.QGroupBox(self.frame_folder)
        self.groupBox_execute.setTitle("")
        self.groupBox_execute.setObjectName("groupBox_execute")
        self.gridLayout_5 = QtWidgets.QGridLayout(self.groupBox_execute)
        self.gridLayout_5.setObjectName("gridLayout_5")
        self.btn_convert = QtWidgets.QPushButton(self.groupBox_execute)
        self.btn_convert.setMaximumSize(QtCore.QSize(182, 82))
        self.btn_convert.setObjectName("btn_convert")
        self.gridLayout_5.addWidget(self.btn_convert, 0, 0, 1, 1)
        self.label_status = QtWidgets.QLabel(self.groupBox_execute)
        self.label_status.setMaximumSize(QtCore.QSize(10182, 182))
        self.label_status.setAlignment(QtCore.Qt.AlignCenter)
        self.label_status.setObjectName("label_status")
        self.gridLayout_5.addWidget(self.label_status, 1, 0, 1, 1)
        self.gridLayout_4.addWidget(self.groupBox_execute, 3, 0, 1, 1)
        self.group_optionals = QtWidgets.QGroupBox(self.frame_folder)
        self.group_optionals.setObjectName("group_optionals")
        self.gridLayout_6 = QtWidgets.QGridLayout(self.group_optionals)
        self.gridLayout_6.setObjectName("gridLayout_6")
        self.dateEdit_date = QtWidgets.QDateEdit(self.group_optionals)
        self.dateEdit_date.setMinimumDate(QtCore.QDate(2000, 1, 1))
        self.dateEdit_date.setDateTime(QtCore.QDateTime.currentDateTime()) # Mine
        self.dateEdit_date.setObjectName("dateEdit_date")
        self.gridLayout_6.addWidget(self.dateEdit_date, 0, 0, 1, 1)
        self.input_comment = QtWidgets.QLineEdit(self.group_optionals)
        self.input_comment.setObjectName("input_comment")
        self.gridLayout_6.addWidget(self.input_comment, 1, 0, 1, 1)
        self.gridLayout_4.addWidget(self.group_optionals, 1, 0, 1, 1)
        self.horizontalLayout.addWidget(self.frame_folder)
        self.tabs_main.addTab(self.tab_folder, "")
        self.gridLayout.addWidget(self.tabs_main, 1, 0, 1, 1)
        self.label_footer = QtWidgets.QLabel(self.centralwidget)
        self.label_footer.setAlignment(QtCore.Qt.AlignCenter)
        self.label_footer.setObjectName("label_footer")
        self.gridLayout.addWidget(self.label_footer, 2, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 506, 20))
        self.menubar.setObjectName("menubar")
        self.menu_File = QtWidgets.QMenu(self.menubar)
        self.menu_File.setObjectName("menu_File")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.action_Quit = QtWidgets.QAction(MainWindow)
        self.action_Quit.setObjectName("action_Quit")
        self.actionReset_Input_Boxes = QtWidgets.QAction(MainWindow)
        self.actionReset_Input_Boxes.setObjectName("actionReset_Input_Boxes")
        self.menu_File.addAction(self.action_Quit)
        self.menu_File.addAction(self.actionReset_Input_Boxes)
        self.menubar.addAction(self.menu_File.menuAction())

        self.retranslateUi(MainWindow)
        self.tabs_main.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        bridges() # Mine

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "EAC"))
        self.label_title.setText(_translate("MainWindow", "ESR ASCII CONVERTER"))
        self.group_selectFile_in.setTitle(_translate("MainWindow", "Input File"))
        self.btn_selectFile_in.setText(_translate("MainWindow", "Select File"))
        self.group_optionals_2.setTitle(_translate("MainWindow", "Optionals"))
        self.dateEdit_date_2.setDisplayFormat(_translate("MainWindow", "dd-MM-yyyy"))
        self.input_comment_2.setPlaceholderText(_translate("MainWindow", "Single line comment"))
        self.group_selectFolder_out_2.setTitle(_translate("MainWindow", "Output file"))
        self.btn_selectFolder_out_2.setText(_translate("MainWindow", "Select Folder"))
        self.label_fileName.setText(_translate("MainWindow", "File name"))
        self.comboBox_format.setItemText(0, _translate("MainWindow", ".esr"))
        self.comboBox_format.setItemText(1, _translate("MainWindow", ".json"))
        self.btn_convert_2.setText(_translate("MainWindow", "Convert"))
        self.label_status_2.setText(_translate("MainWindow", "Ready."))
        self.tabs_main.setTabText(self.tabs_main.indexOf(self.tab_single_file), _translate("MainWindow", "Single File Mode"))
        self.group_selectFolder_in.setTitle(_translate("MainWindow", "Select Input Folder"))
        self.btn_selectFolder_in.setText(_translate("MainWindow", "Select Folder"))
        self.group_selectFolder_out.setTitle(_translate("MainWindow", "Select Output Folder"))
        self.btn_selectFolder_outFolder.setText(_translate("MainWindow", "Select Folder"))
        self.comboBox_folderFormat.setItemText(0, _translate("MainWindow", "esr"))
        self.comboBox_folderFormat.setItemText(1, _translate("MainWindow", "json"))
        self.btn_convert.setText(_translate("MainWindow", "Convert"))
        self.label_status.setText(_translate("MainWindow", "Ready."))
        self.group_optionals.setTitle(_translate("MainWindow", "Optionals"))
        self.dateEdit_date.setDisplayFormat(_translate("MainWindow", "dd-MM-yyyy"))
        self.input_comment.setPlaceholderText(_translate("MainWindow", "Single line comment"))
        self.tabs_main.setTabText(self.tabs_main.indexOf(self.tab_folder), _translate("MainWindow", "Folder Mode"))
        self.label_footer.setText(_translate("MainWindow", "EAC was </> with <3 by Giovanni Crisalfi"))
        self.menu_File.setTitle(_translate("MainWindow", "File"))
        self.action_Quit.setText(_translate("MainWindow", "Quit"))
        self.actionReset_Input_Boxes.setText(_translate("MainWindow", "Reset Input Boxes"))
